package ljs;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import helloworld.GreeterGrpc.GreeterBlockingStub;
import helloworld.GreeterGrpc;
import helloworld.HelloReply;
import helloworld.HelloRequest;

import java.util.Iterator;

public class HelloWorldClient {

    private final GreeterBlockingStub blockingStub;

    public HelloWorldClient(String host, int port) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext()  // You should use encryption in a production setting
                .build();
        blockingStub = GreeterGrpc.newBlockingStub(channel);
    }

    public void greet(String name) {
        HelloRequest request = HelloRequest.newBuilder().setName(name).build();
        HelloReply response;

        try {
            response = blockingStub.sayHello(request);
            System.out.println("Server response: " + response.getMessage());
        } catch (Exception e) {
            System.err.println("RPC failed: " + e.getMessage());
        }
    }

    public void greetWithStream(String name) {
        HelloRequest request = HelloRequest.newBuilder().setName(name).build();

        try {
            Iterator<HelloReply> responses = blockingStub.sayHelloStream(request);
            while (responses.hasNext()) {
                HelloReply reply = responses.next();
                System.out.println("Streamed response: " + reply.getMessage());
            }
        } catch (Exception e) {
            System.err.println("RPC failed: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: HelloWorldClient <host> <port>");
            System.exit(1);
        }

        String host = args[0];
        int port = Integer.parseInt(args[1]);

        System.out.println("Client is configured to connect to server on - " + host + ":" + port);

        HelloWorldClient client = new HelloWorldClient(host, port);
        client.greet("world");
        client.greetWithStream("world-stream");
    }
}