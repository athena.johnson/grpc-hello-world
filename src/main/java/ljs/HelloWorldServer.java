package ljs;

import helloworld.GreeterGrpc;
import helloworld.HelloReply;
import helloworld.HelloRequest;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

public class HelloWorldServer {
    static int DEFAULT_PORT = 8080;
    static int CONFIG_PORT = 9090;
    static boolean USE_DETERMINATE_LOOP = true;
    static int messagesPerSecond = 10;
    static long delayBetweenMessagesInMillis = 1000 / messagesPerSecond;

    public static void main(String[] args) throws Exception {
        Server server = ServerBuilder.forPort(CONFIG_PORT)
                .addService(new GreeterImpl())
                .build()
                .start();

        System.out.println("Server started, listening on " + CONFIG_PORT);

        server.awaitTermination();
    }

    static class GreeterImpl extends GreeterGrpc.GreeterImplBase {
        @Override
        public void sayHello(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            HelloReply reply = HelloReply.newBuilder().setMessage("Hello " + req.getName()).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void sayHelloStream(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            if(USE_DETERMINATE_LOOP)
            {
                for (int i = 0; i < 5; i++) {
                    HelloReply reply = HelloReply.newBuilder().setMessage("Hello " + req.getName() + ", response " + (i+1)).build();
                    responseObserver.onNext(reply);
                }
                responseObserver.onCompleted();
            }
            else{
                int i = 0;
                // Print a message 10 times per second
                while (true) {
                    HelloReply reply = HelloReply.newBuilder().setMessage("Hello " + req.getName() + ", response " + (i++)).build();
                    responseObserver.onNext(reply);
                    try {
                        Thread.sleep(delayBetweenMessagesInMillis);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        System.err.println("Thread interrupted: " + e.getMessage());
                    }
                }
            }
            responseObserver.onCompleted();
        }
    }
}
