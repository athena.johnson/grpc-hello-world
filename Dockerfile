# FROM azul/zulu-openjdk:17-latest
FROM arm64v8/openjdk:17-slim
# FROM openjdk:17.0.1

WORKDIR /app

# Copying .jar file from target directory to the current directory
COPY ./build/libs/hello-world-server-0.1.0.jar /app/helloworld.jar

ENTRYPOINT ["java", "-jar", "helloworld.jar"]