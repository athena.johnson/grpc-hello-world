# grpc-hello-world


## Getting started

This is a very simple example that is showcasing a gRPC Client that will connect to a gRPC Server to send a regular 
Unary call (single Request - then Response flow) as well as a Server-Streaming example (single Request then stream of
Responses from Server). This is meant to be a quick and easy test setup.

-   HelloWorldClient
-   HelloWorldServer

## Running the Example

-   Running the Server

The server is configured to run on a default port but this can be changed in the code.

-   Running the Client

The client must be run with commandline args providing the IP and the port of the server it should connect to.

## Deploying Docker

First build the fat jar of the server through the gradle script
```
./gradlew shadowJar  
```
Then you can build the docker image that will contain the fat jar. This command is using buildx
because we are targeting pushing and running the docker image on an ARM system.

```
docker buildx build --platform linux/amd64,linux/arm64 -t ljsolutions/helloworld:latest --push .
```